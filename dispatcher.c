#define _GNU_SOURCE
#define _XOPEN_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sysexits.h>
#include <unistd.h>

enum{
    // Determines how many messages to send to the queue
    MAX_LISTNERS = 12,
    // Authrorization level for the queue
    // (Special Permis,User, Group, Others)
    AUTH = 0644,
    BUFFER_SIZE = 256
};

// Guarenteed to have only one /etc per system
#define QUEUE_PATH "/etc"

// Struct taken from Beej's IPC guide for creating Message Queues
struct message_buffer {
    long mtype;
    char message[BUFFER_SIZE];
};

// Message Queue based on code given in class from Beej's guide
// Chapter 7 Message Queues.
int main(void)
{
    struct message_buffer relay;

    // Building the key 
    key_t key = ftok(QUEUE_PATH, 'B');
    if(key == -1) {
        perror("ftok");
        return(EX_CANTCREAT);
    }

    // Build the queue ensuring proper Authorizations
    // using AUTH ORed with IPC_CREAT to create queue
    int msg_id = msgget(key, AUTH | IPC_CREAT);
    if(msg_id == -1) {
        perror("msgget");
        return(EX_CANTCREAT);
    }
    
    // Not used, but the struct is called for be msgrcv()/msgsnd()
    relay.mtype = 1; 

    // Helpful hint to user
    printf("Enter lines of text, ^D to quit:\n");
    // This looping allows CTRL+D to exit
    while(printf("> "), fgets(relay.message, sizeof(relay.message),
                              stdin) != NULL){
        // Sending message to queue for max amount of listners
        for(int i = 0; i < MAX_LISTNERS; ++i){
            if(msgsnd(msg_id, &relay, (strlen(relay.message) + 1), 0) == -1){
                perror("msgsnd");
                return(EX_PROTOCOL);
            }
        }
        // Ensuring there is not erronious data in relay.message
        for(int i = 0; i < BUFFER_SIZE; ++i){
            relay.message[i] = '\0';
        }
        // Sleeping to ensure all listeners can grab the message
        usleep(35000);

        // Destroy all tracers of a queue
        // Idea for queue destructioon taken from S. Cuillo
        // using msgctl IPC_RMID to remove message queue
        if(msgctl(msg_id, IPC_RMID, NULL) == -1) {
            perror("Message Queue Deletion");
            return(EX_UNAVAILABLE);
        }

        // We built the Queue one time, let's build it again
        // using AUTH ORed with IPC_CREAT to create queue
        if((msg_id = msgget(key, AUTH | IPC_CREAT)) == -1){
            perror("msgget");
            return(EX_CANTCREAT);
        }
    }

    // Final clean up of Queue
    if(msgctl(msg_id, IPC_RMID, NULL) == -1){
        perror("msgctl");
        return(EX_UNAVAILABLE);
    }
    printf("Dispatcher Signing Off..\n");
    return(EX_OK);
}
