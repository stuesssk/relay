#define _GNU_SOURCE
#define _XOPEN_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sysexits.h>
#include <unistd.h>


enum{
    BUFFER_SIZE = 256,
    AUTH = 0644
};

#define QUEUE_PATH "/etc"

// Struct called for by msgrcv
struct message_buffer {
    long type;
    char message[BUFFER_SIZE];
};


// Code based on Beej's Guide ch7 on message queues, given in class
int main(void)
{
    struct message_buffer relay;

    // Ensure we have the smae key as the dispatcher
    key_t key = ftok(QUEUE_PATH, 'B');
    if(key == -1) {
        perror("ftok");
        return(EX_CANTCREAT);
    }

    // Not used, but the struct is called for be msgrcv()/msgsnd()
    relay.type = 1;
    // Loop until Dispatcher dies, because while(1)/While(true)
    // is not good enough
    for(;;){
        // Reconnect to the new queue, since we are destroying
        // it after each message
        int msg_id = msgget(key, AUTH);
        if(msg_id == -1) {
            perror("msgget");
            return(EX_CANTCREAT);
        }

        // Grab Message but if Dispatcher does not exist,
        // no need to exist ourselves
        if(msgrcv(msg_id, &relay, BUFFER_SIZE, 0, (IPC_NOWAIT & 0)) == -1){
            printf("Dispatcher cease to exist, Exiting Program.\n");
            break;
        }

        // Strip the newline from the message we'll add our own if needed
        if(relay.message[strlen(relay.message) - 1] == '\n'){
            relay.message[strlen(relay.message) - 1] = '\0';
        }
        // The buisness end of the program, printing the message from
        //  dispatch
        printf("Relay Message: %s\n", relay.message);

        // Ensuring there is not erronious data in relay.message
        for(int i = 0; i < BUFFER_SIZE; ++i){
            relay.message[i] = '\0';
        }
        // Sleeping to give other listeners chance at the message
        usleep(55000);
    }
    return(EX_OK);
}
