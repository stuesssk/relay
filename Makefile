CPPFLAGS+=-Wall -Wextra -Wpedantic
CPPFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal
CPPFLAGS+=-Waggregate-return -Winline

CFLAGS+=-std=c11

LDLIBS+=-lm
LDLIBS+=-pthread

BINS=dispatcher listener
all: $(BINS)

dispatcher: dispatcher.o
listener: listener.o
.PHONY: clean debug profile

$(BIN): $(OBJS)

debug: CFLAGS+=-g
debug: all

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: $(BINS)

clean:
	$(RM) $(BINS) *.o
